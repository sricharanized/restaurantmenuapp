# restaurantMenuApp - A minimalist data driven web application. #


### What is this repository for? ###

A simple web app that shows basic paradigms of scalable web application development.

### Features ###

* Follows CRUD rules of persistant storage
* Native web server in python
* Uses an ORM mapper
* A friendly public API
* No heavy frameworks used, built on python
* Unit tests
* Deployment friendly

### Further plans ###

* A distributive storage backend written in C++ for high performance